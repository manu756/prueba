var expect = require('chai').expect;
var writeExample = require('../writeExample');

describe('writeExample()', function () {
  it('should return \"AAA\" string', function () {
    
    // 1. ARRANGE
    var expectedString = 'AAA';

    // 2. ACT
    var returnedString = writeExample();

    // 3. ASSERT
    expect(returnedString).to.be.equal(expectedString);

  });
});